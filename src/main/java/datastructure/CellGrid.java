package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    /**
     * Defines the width of the grid
     */
    private int cols;

    /**
     * Defines the height of the grid
     */
    private int myRows;

    /**
     * Array grid that can hold rows*columns CellStates
     */
    private CellState[][] AllStates;

    private CellState currentState;

    private CellGrid copiedGrid;

    private CellState[][] CopiedAllStates;



    public CellGrid(int rows, int columns, CellState initialState) {
		this.myRows = rows;
        this.cols = columns;
        this.currentState = initialState;
        AllStates = new CellState[rows][columns];
        CopiedAllStates = new CellState[rows][columns];
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                AllStates[i][j] = initialState;
            }
        }
        
	}

    

    @Override
    public int numRows() {
        return this.myRows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 ) {
            throw new IndexOutOfBoundsException(); 
        }
        if (row > numRows() + 1) {
            throw new IndexOutOfBoundsException(); 
        }
        if (column < 0) {
            throw new IndexOutOfBoundsException(); 
        }
        if (column > numColumns() + 1) {
            throw new IndexOutOfBoundsException(); 
        }
        AllStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 ) {
            throw new IndexOutOfBoundsException(); 
        }
        if (row > numRows() + 1) {
            throw new IndexOutOfBoundsException(); 
        }
        if (column < 0) {
            throw new IndexOutOfBoundsException(); 
        }
        if (column > numColumns() + 1) {
            throw new IndexOutOfBoundsException(); 
        }
        return AllStates[row][column];
    }

    @Override
    public IGrid copy() {
        copiedGrid = new CellGrid(this.numRows(), this.numColumns(), currentState);
        for (int a = 0; a < this.numRows(); a++){
            for (int b = 0; b < this.numColumns(); b++){
                copiedGrid.set(a, b, this.get(a, b));
            }
        }
        return copiedGrid;
    }
    
}
